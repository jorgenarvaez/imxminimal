/////////////////////////////
// Ripple Animation Effect //
/////////////////////////////

/////////////////
// Base Ripple //
/////////////////
$(".btn-rippled").click(function(e){
  var parent, ink, d, x, y;
  parent = $(this).parent();
  //create .ink element if it doesn't exist
  if(parent.find(".ink").length == 0)
    parent.prepend("<span class='ink'></span>");
      
  ink = parent.find(".ink");
  //incase of quick double clicks stop the previous animation
  ink.removeClass("animate");
  
  //set size of .ink
  if(!ink.height() && !ink.width())
  {
    //use parent's width or height whichever is larger for the diameter to make a circle which can cover the entire element.
    d = Math.max(parent.outerWidth(), parent.outerHeight());
    ink.css({height: d, width: d});
  }
  
  //get click coordinates
  //logic = click coordinates relative to page - parent's position relative to page - half of self height/width to make it controllable from the center;
  x = e.pageX - parent.offset().left - ink.width()/2;
  y = e.pageY - parent.offset().top - ink.height()/2;
  
  //set the position and add class .animate
  ink.css({top: y+'px', left: x+'px'}).addClass("animate");
});

//////////////////
// Green Ripple //
//////////////////
$(".btn-rippled-green").click(function(e){
    var parent, ink, d, x, y;
    parent = $(this).parent();
    //create .ink element if it doesn't exist
    if(parent.find(".ink-green").length == 0)
        parent.prepend("<span class='ink-green'></span>");
        
    ink = parent.find(".ink-green");
    //incase of quick double clicks stop the previous animation
    ink.removeClass("animate");
    
    //set size of .ink
    if(!ink.height() && !ink.width())
    {
        //use parent's width or height whichever is larger for the diameter to make a circle which can cover the entire element.
        d = Math.max(parent.outerWidth(), parent.outerHeight());
        ink.css({height: d, width: d});
    }
    
    //get click coordinates
    //logic = click coordinates relative to page - parent's position relative to page - half of self height/width to make it controllable from the center;
    x = e.pageX - parent.offset().left - ink.width()/2;
    y = e.pageY - parent.offset().top - ink.height()/2;
    
    //set the position and add class .animate
    ink.css({top: y+'px', left: x+'px'}).addClass("animate");
});

////////////////
//Blue Ripple //
////////////////
$(".btn-rippled-blue").click(function(e){
    var parent, ink, d, x, y;
    parent = $(this).parent();
    //create .ink element if it doesn't exist
    if(parent.find(".ink-blue").length == 0)
        parent.prepend("<span class='ink-blue'></span>");
        
    ink = parent.find(".ink-blue");
    //incase of quick double clicks stop the previous animation
    ink.removeClass("animate");
    
    //set size of .ink
    if(!ink.height() && !ink.width())
    {
        //use parent's width or height whichever is larger for the diameter to make a circle which can cover the entire element.
        d = Math.max(parent.outerWidth(), parent.outerHeight());
        ink.css({height: d, width: d});
    }
    
    //get click coordinates
    //logic = click coordinates relative to page - parent's position relative to page - half of self height/width to make it controllable from the center;
    x = e.pageX - parent.offset().left - ink.width()/2;
    y = e.pageY - parent.offset().top - ink.height()/2;
    
    //set the position and add class .animate
    ink.css({top: y+'px', left: x+'px'}).addClass("animate");
});

///////////////////
// Orange Ripple //
///////////////////
$(".btn-rippled-orange").click(function(e){
    var parent, ink, d, x, y;
    parent = $(this).parent();
    //create .ink element if it doesn't exist
    if(parent.find(".ink-orange").length == 0)
        parent.prepend("<span class='ink-orange'></span>");
        
    ink = parent.find(".ink-orange");
    //incase of quick double clicks stop the previous animation
    ink.removeClass("animate");
    
    //set size of .ink
    if(!ink.height() && !ink.width())
    {
        //use parent's width or height whichever is larger for the diameter to make a circle which can cover the entire element.
        d = Math.max(parent.outerWidth(), parent.outerHeight());
        ink.css({height: d, width: d});
    }
    
    //get click coordinates
    //logic = click coordinates relative to page - parent's position relative to page - half of self height/width to make it controllable from the center;
    x = e.pageX - parent.offset().left - ink.width()/2;
    y = e.pageY - parent.offset().top - ink.height()/2;
    
    //set the position and add class .animate
    ink.css({top: y+'px', left: x+'px'}).addClass("animate");
});

//////////////////
// White Ripple //
//////////////////
$(".btn-rippled-white").click(function(e){
    var parent, ink, d, x, y;
    parent = $(this).parent();
    //create .ink element if it doesn't exist
    if(parent.find(".ink-white").length == 0)
        parent.prepend("<span class='ink-white'></span>");
        
    ink = parent.find(".ink-white");
    //incase of quick double clicks stop the previous animation
    ink.removeClass("animate");
    
    //set size of .ink
    if(!ink.height() && !ink.width())
    {
        //use parent's width or height whichever is larger for the diameter to make a circle which can cover the entire element.
        d = Math.max(parent.outerWidth(), parent.outerHeight());
        ink.css({height: d, width: d});
    }
    
    //get click coordinates
    //logic = click coordinates relative to page - parent's position relative to page - half of self height/width to make it controllable from the center;
    x = e.pageX - parent.offset().left - ink.width()/2;
    y = e.pageY - parent.offset().top - ink.height()/2;
    
    //set the position and add class .animate
    ink.css({top: y+'px', left: x+'px'}).addClass("animate");
});


//////////////////////////////
// Display Navbar on Scroll //
//////////////////////////////

$(window).scroll(function () {
    var elem = $('#main-nav');
    setTimeout(function() {
        elem.css({"opacity":"0","transition":"1s"});
    },5000);            
    elem.css({"opacity":"1","transition":"0.5s"});    
});